package org.unibo.crypto.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unibo.crypto.algo.SymmetricCryptoEngine;

public class AesEncTest {

    private SymmetricCryptoEngine symmetricCrypto;

    @Before
    public void before(){
        symmetricCrypto = new SymmetricCryptoEngine();
    }

    @Test
    public void TestEncDecc(){
        String text = "Questa è una prova di cifratura";
        String encText = symmetricCrypto.encrypt(text);
        System.out.println("Encrypted text is " + encText);
        String clearText =symmetricCrypto.decrypt(encText);
        System.out.println("Decrypted text is " + clearText);
        Assert.assertEquals(text,clearText);
    }
}
