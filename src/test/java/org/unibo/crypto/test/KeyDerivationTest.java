package org.unibo.crypto.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unibo.crypto.algo.KeyDerivationEngine;
import org.unibo.crypto.algo.KeyGen;
import org.unibo.crypto.algo.SymmetricCryptoEngine;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class KeyDerivationTest {

    private final static int iterationNumber = 10000;
    private KeyDerivationEngine keyDerivation;


    @Before
    public void before(){
        this.keyDerivation = new KeyDerivationEngine(iterationNumber );
    }

    @Test
    public void TestWithAes(){
        byte[] salt  = KeyGen.generateSalt();
        SecretKey key = this.keyDerivation.generateKey("T3sts1cur3zz4!",salt);
        SymmetricCryptoEngine aesCrypto = new SymmetricCryptoEngine(key);

        String initialText = "Questo è un test, la stringa non deve essere comunque troppo corta";
        String encodedText = aesCrypto.encrypt(initialText);
        System.out.println("Encoded text is " + encodedText);
        String finalText = aesCrypto.decrypt(encodedText);

        Assert.assertEquals(initialText,finalText);
    }

    @Test
    public void TestMultipleInstances(){
        byte[] salt = KeyGen.generateSalt();
        SecretKey key = this.keyDerivation.generateKey("T3sts1cur3zz4!",salt);
        SymmetricCryptoEngine aesCrypto = new SymmetricCryptoEngine(key);
        String initialText = "Questo è un test, la stringa non deve essere comunque troppo corta";
        String encodedText = aesCrypto.encrypt(initialText);

        KeyDerivationEngine secondInstance = new KeyDerivationEngine(iterationNumber);
        SymmetricCryptoEngine aesCryptoFinal = new SymmetricCryptoEngine(secondInstance.generateKey("T3sts1cur3zz4!",salt),aesCrypto.getAlgoParameterSpec());
        String finalText = aesCryptoFinal.decrypt(encodedText);

        Assert.assertEquals(initialText,finalText);
    }
}
