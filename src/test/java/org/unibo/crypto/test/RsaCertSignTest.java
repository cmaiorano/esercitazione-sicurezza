package org.unibo.crypto.test;

import org.junit.Assert;
import org.junit.Test;
import org.unibo.crypto.algo.RsaSignatureWithCertificateEngine;

public class RsaCertSignTest {

    @Test
    public void test(){
        RsaSignatureWithCertificateEngine engine = new RsaSignatureWithCertificateEngine();
        engine.init("esercitazione.jks");

        String text = "ciao questo è un test";
        byte[] signature = engine.sign(text);
        boolean verified = engine.verify(text,signature);

        Assert.assertTrue(verified);
    }
}
