package org.unibo.crypto.test;

import org.bouncycastle.util.encoders.Base64;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unibo.crypto.algo.RsaEncryptionEngine;
import org.unibo.crypto.algo.SymmetricCryptoEngine;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//RSA Public key encryption typically will be used to cipher ephemeral symmetric keys used for target data encryption
//In order to demonstrate this we will define a complex use case where we want to exchange a
public class RsaEncTest {

    private RsaEncryptionEngine rsaPublicKeyEnc;

    @Before
    public void before() {
        this.rsaPublicKeyEnc = new RsaEncryptionEngine(2048); //2048 bits is the recommended keysize for now
    }

    @Test
    public void TestEnc() {
        String initialText = "Questo è il solito test di sicurezza, utilizziamo una stringa arbitrariamente"
                                + "lunga per dimostrare che il meccanismo di crittografia funziona (se tutto va bene)";

        //Defining first symmetric stuff
        SymmetricCryptoEngine symmetricCryptoInitial = new SymmetricCryptoEngine();
        SecretKey key = symmetricCryptoInitial.getKey(); //We want to save key in order to simulate the decryption on another device
        byte[] iv = symmetricCryptoInitial.getAlgoParameterSpec().getIV(); //We want also to save initialization vector (in this case I'm saving the entire spec object)
        byte[] encKey = this.rsaPublicKeyEnc.encrypt(key.getEncoded()); //This is the encrypted
        String encText = symmetricCryptoInitial.encrypt(initialText);

        System.out.println("Encrypted text is " + encText);
        System.out.println("Encrypted and encoded base64 key is "+ Base64.toBase64String(encKey));
        System.out.println("Encrypted and encoded base64 iv is "+ Base64.toBase64String(iv));

        byte[] decodedKey = this.rsaPublicKeyEnc.decrypt(encKey);
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        SymmetricCryptoEngine symmetricCryptoFinal = new SymmetricCryptoEngine(originalKey.getEncoded(),iv);
        String finalText = symmetricCryptoFinal.decrypt(encText);
        Assert.assertEquals(initialText, finalText);
    }

}
