package org.unibo.crypto.test;

import org.bouncycastle.util.encoders.Base64;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unibo.crypto.algo.RsaSignatureEngine;

public class RsaSigTest {

    @Test
    public void testSignatureWithAppendix(){
        RsaSignatureEngine engine = new RsaSignatureEngine(2048);

        String textToBeSigned = "Questo testo verrà firmato digitalmente e successivamente verificato";
        byte[] sign = engine.sign(textToBeSigned);
        System.out.println("Signature size " + sign.length*8 + " Original text size " + textToBeSigned.getBytes().length * 8);
        System.out.println("Encoded base64 signature is "+ Base64.toBase64String(sign));
        boolean verified = engine.verify(textToBeSigned.getBytes(),sign);

        Assert.assertTrue(verified);
    }
}
