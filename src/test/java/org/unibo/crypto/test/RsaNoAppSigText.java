package org.unibo.crypto.test;

import org.bouncycastle.util.encoders.Base64;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unibo.crypto.algo.RsaSignatureNoAppendixEngine;

public class RsaNoAppSigText {

    private static RsaSignatureNoAppendixEngine engine;

    @BeforeClass
    public static void before(){
        engine = new RsaSignatureNoAppendixEngine(2048);
    }

    @Test
    public void Test(){
        String plainText = "Questa è una stringa";
        byte[] result = engine.sign(plainText);
        System.out.println("Out text is " + Base64.toBase64String(result));
        boolean verify = engine.verify(plainText,result);

        Assert.assertTrue(verify);
    }

    @Test
    public void TestLongerString(){
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas commodo auctor nibh vel iaculis. In hac habitasse platea dictumst. Curabitur vel lectus malesuada, porttitor mi et, accumsan diam. Suspendisse id sem libero. Praesent accumsan placerat sodales. Nulla facilisi. Quisque risus eros, mattis porttitor urna ac, sagittis ornare augue.";
        byte[] result = engine.sign(text);
        System.out.println("Out text is " + Base64.toBase64String(result) + " and its lenght is " + result.length + " and original text lenght is " + text.getBytes().length);
        boolean verified = engine.verify(text,result);
        Assert.assertTrue(verified);
    }
}
