package org.unibo.crypto.algo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.*;

public class RsaSignatureEngine {

    private KeyPair keyPair;
    private Signature signature;

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    public RsaSignatureEngine(int keySize) {
        try {
            this.signature = Signature.getInstance("SHA256withRSA");
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
            generator.initialize(keySize);
            this.keyPair = generator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] sign(String data) {
        try {
            this.signature.initSign(this.keyPair.getPrivate());
            this.signature.update(data.getBytes());
            return this.signature.sign();
        } catch (InvalidKeyException | SignatureException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean verify(byte[] data, byte[] signature) {
        try {
            this.signature.initVerify(this.keyPair.getPublic());
            this.signature.update(data);
            return this.signature.verify(signature);
        } catch (InvalidKeyException | SignatureException e) {
            throw new RuntimeException(e);
        }
    }
}
