package org.unibo.crypto.algo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class RsaEncryptionEngine {
    private KeyPair keyPair;
    private Cipher cipher;

    static{
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    public RsaEncryptionEngine(int keyLenght){
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
            this.cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA256ANDMGF1PADDING", BouncyCastleProvider.PROVIDER_NAME); //this version is bounded with bouncy castle RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING is provided with SUN JCE provider instead
            keyPairGenerator.initialize(keyLenght);
            this.keyPair = keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e){
            throw new RuntimeException(e);
        }
    }

    public RsaEncryptionEngine(String keystoreLocation){
        try {
            InputStream keystoreStream = ClassLoader.getSystemClassLoader().getResourceAsStream(keystoreLocation);
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(keystoreStream, "sicurezzam".toCharArray());
            String alias = "1"; //alias is the identifier of target key and certificate in the keystore

            Key key = ks.getKey(alias,"sicurezzam".toCharArray());
            if (key instanceof PrivateKey){
                Certificate cert = ks.getCertificate(alias); //I've build the keystore with key and certificate with the same name
                this.keyPair = new KeyPair(cert.getPublicKey(),(PrivateKey)key);
            } else {
                throw new RuntimeException("No valid key");
            }

            this.cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA256ANDMGF1PADDING", BouncyCastleProvider.PROVIDER_NAME);
        } catch (KeyStoreException | IOException | UnrecoverableKeyException | CertificateException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e){
            throw new RuntimeException(e);
        }
    }

    public byte[] encrypt(byte[] clearText){
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
        }catch (InvalidKeyException e){
            throw new RuntimeException(e);
        }
        return this.getEncodedText(clearText);
    }

    public byte[] decrypt(byte[] cipherText){
        try {
            this.cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        } catch (InvalidKeyException e){
            throw new RuntimeException(e);
        }
        return this.getEncodedText(cipherText);
    }

    private byte[] getEncodedText(byte[] text){
        try {
            return this.cipher.doFinal(text);
        } catch (BadPaddingException | IllegalBlockSizeException e){
            throw new RuntimeException(e); //We always want an unchecked exception, they are beautiful
        }
    }


}
