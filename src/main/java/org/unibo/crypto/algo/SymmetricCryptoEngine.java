package org.unibo.crypto.algo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;

public class SymmetricCryptoEngine {

    private IvParameterSpec algoParameterSpec;
    private Cipher cipher;
    private SecretKey key;

    static {
       if(Security.getProvider(BouncyCastleProvider.PROVIDER_NAME)==null){
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    public SymmetricCryptoEngine() {

        this.algoParameterSpec = KeyGen.generateIv();
        try {
            cipher = Cipher.getInstance("AES/CBC/pkcs5padding", BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchProviderException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e); //always a nice exeception handling
        }
        key = KeyGen.generateAesKeys();
    }
    public SymmetricCryptoEngine(SecretKey key){
        this.algoParameterSpec = KeyGen.generateIv();
        try {
            cipher = Cipher.getInstance("AES/CBC/pkcs5padding", BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchProviderException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
        this.key = key;
    }

    public SymmetricCryptoEngine(byte[] key, byte[] iv){
        this.algoParameterSpec = new IvParameterSpec(iv);
        try {
            cipher = Cipher.getInstance("AES/CBC/pkcs5padding", BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchProviderException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
        this.key = new SecretKeySpec(key, "AES");
    }


    public IvParameterSpec getAlgoParameterSpec() {
        return algoParameterSpec;
    }

    public SecretKey getKey(){
        return this.key;
    }

    public String encrypt(String clearText) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, algoParameterSpec);
            byte[] encText = cipher.doFinal(clearText.getBytes());
            return Base64.toBase64String(encText);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }

    public String decrypt(String b64EncText) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, key, algoParameterSpec);
            byte[] decText = cipher.doFinal(Base64.decode(b64EncText));
            return new String(decText);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }

    public String encrypt(byte[] inputBytes) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, algoParameterSpec);
            byte[] encText = cipher.doFinal(inputBytes);
            return Base64.toBase64String(encText);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }
}
