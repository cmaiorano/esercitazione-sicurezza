package org.unibo.crypto.algo;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

public class KeyGen {

    private final static int seedBytes = 20;


    public static SecretKeySpec generateAesKeys(){
        SecureRandom random = new SecureRandom();
        byte[] keyBytes = new byte[16];
        random.nextBytes(keyBytes);
        return new SecretKeySpec(keyBytes, "AES");
    }

    public static IvParameterSpec generateIv(){
        SecureRandom random = new SecureRandom();
        byte[] ivBytes = new byte[16];
        random.nextBytes(ivBytes);
        return new IvParameterSpec(ivBytes);
    }


    public static byte[] generateSalt(){
        SecureRandom random = new SecureRandom();
        return random.generateSeed(seedBytes);
    }


}
