package org.unibo.crypto.algo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class RsaSignatureWithCertificateEngine {

    private PrivateKey privateKey;
    private PublicKey publicKey;
    private Signature signature;

    static{
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null){
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public void init(String keystoreLocation){
        try {
            InputStream keystoreStream = ClassLoader.getSystemClassLoader().getResourceAsStream(keystoreLocation);
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(keystoreStream, "sicurezzam".toCharArray());
            String alias = "1"; //alias is the identifier of target key and certificate in the keystore

            Key key = ks.getKey(alias,"sicurezzam".toCharArray());
            if (key instanceof PrivateKey){
                Certificate cert = ks.getCertificate(alias); //I've build the keystore with key and certificate with the same name
                this.publicKey = cert.getPublicKey();
                this.privateKey = (PrivateKey)key;
            } else {
                throw new RuntimeException("No valid key");
            }

            this.signature = Signature.getInstance("SHA256withRSA", BouncyCastleProvider.PROVIDER_NAME);

        } catch (KeyStoreException | IOException | UnrecoverableKeyException | CertificateException | NoSuchAlgorithmException | NoSuchProviderException e){
            throw new RuntimeException(e);
        }
    }

    public byte[] sign(String text){
        try {
            this.signature.initSign(this.privateKey);
            this.signature.update(text.getBytes());
            return signature.sign();
        } catch (InvalidKeyException | SignatureException e){
            throw new RuntimeException(e);
        }
    }

    public boolean verify(String originalText, byte[] signatureBytes){
        try{
            this.signature.initVerify(this.publicKey);
            this.signature.update(originalText.getBytes());
            return this.signature.verify(signatureBytes);
        } catch (InvalidKeyException | SignatureException e){
            throw new RuntimeException(e);
        }
    }
}
