package org.unibo.crypto.algo;

import org.apache.commons.lang3.ArrayUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.event.IIOWriteProgressListener;
import java.security.*;
import java.util.Arrays;

public class RsaSignatureNoAppendixEngine {
    private Cipher cipher;
    private KeyPair keyPair;

    static{
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null){
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public RsaSignatureNoAppendixEngine(int keyLenght){
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
            this.cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA256ANDMGF1PADDING", BouncyCastleProvider.PROVIDER_NAME);
            keyPairGenerator.initialize(keyLenght);
            this.keyPair = keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e){
            throw new RuntimeException(e);
        }
    }

    public int getBlockSize(){
        return this.cipher.getBlockSize();
    }

    public byte[] sign(String text){
        try{
            this.cipher.init(Cipher.ENCRYPT_MODE,this.keyPair.getPrivate());
            byte[] textBytes = text.getBytes();
            int blockSize = this.cipher.getBlockSize();
            if (textBytes.length > blockSize){
                byte[] result = new byte[0];
                for (int i = 0; i < textBytes.length; i += blockSize){
                    int offset = Math.min(textBytes.length - i,blockSize);
                    byte[] tmp = this.cipher.doFinal(textBytes, i, offset);
                    result = ArrayUtils.addAll(result,tmp);
                }
                return result;
            } else {
                return this.cipher.doFinal(text.getBytes());
            }
        } catch(InvalidKeyException | IllegalBlockSizeException | BadPaddingException e){
            throw new RuntimeException(e);
        }
    }

    public boolean verify(String originalText, byte[] signedData){
        try{
            this.cipher.init(Cipher.DECRYPT_MODE,this.keyPair.getPublic());
            byte[] originalTextByes = originalText.getBytes();
            int blockSize = this.cipher.getBlockSize();
            if (originalTextByes.length > blockSize){
                byte[] result = new byte[0];
                for (int i = 0; i < signedData.length; i += blockSize){
                    int offset = Math.min(signedData.length - i, blockSize);
                    result = ArrayUtils.addAll(result,this.cipher.doFinal(signedData,i,offset));
                }
                String verifiedText = new String(result).trim();
                return originalText.equals(verifiedText);
            } else {
                String text = new String(this.cipher.doFinal(signedData)).trim();
                return originalText.equals(text);
            }
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e){
            throw new RuntimeException(e);
        }
    }



}
