package org.unibo.crypto.algo;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class KeyDerivationEngine {

    private int iterationNumber;

    static{
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null){
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public KeyDerivationEngine(int iterationNumber) {
        this.iterationNumber = iterationNumber;

    }
    public SecretKeySpec generateKey(String password, byte[] salt) {
        try {
            SecretKeyFactory kf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256", BouncyCastleProvider.PROVIDER_NAME);

            KeySpec specs = new PBEKeySpec(password.toCharArray(), salt, this.iterationNumber, 128);
            SecretKey key = kf.generateSecret(specs);
            return new SecretKeySpec(key.getEncoded(), "AES");
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e){
            throw new RuntimeException(e);
        }
    }
}
