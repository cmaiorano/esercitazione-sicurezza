package org.unibo.crypto.main;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.unibo.crypto.algo.RsaEncryptionEngine;
import org.unibo.crypto.algo.SymmetricCryptoEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Solution {

    public static void main(String[] args){
        String mode = args[0];
        String filePath = args[1];

        RsaEncryptionEngine asymmetricEngine = new RsaEncryptionEngine("esercitazione.jks");

        if (mode.equalsIgnoreCase("ENCRYPT")) {

            try {
                FileInputStream fis = new FileInputStream(filePath);
                byte[] fileBytes = IOUtils.toByteArray(fis);

                fis.close();
                FileUtils.forceDelete(new File(filePath));

                File encodedFile = new File(filePath);
                String path = encodedFile.getParent();

                SymmetricCryptoEngine engine = new SymmetricCryptoEngine();
                String encodedText = engine.encrypt(fileBytes);
                FileUtils.writeStringToFile(encodedFile,encodedText,"UTF-8");
                byte[] encodedKey = asymmetricEngine.encrypt(engine.getKey().getEncoded());
                byte[] encodedIv = engine.getAlgoParameterSpec().getIV();
                FileUtils.writeByteArrayToFile(new File(path + File.separator + "EncodedIV"),encodedIv);
                FileUtils.writeByteArrayToFile(new File(path + File.separator +"EncodedKey"),encodedKey);
                System.out.println("Encryption process terminated");
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        } else if (mode.equalsIgnoreCase("DECRYPT")){
            try{
                String path = new File(filePath).getParent();
                FileInputStream fileEncoded = new FileInputStream(filePath);
                FileInputStream ivEncoded = new FileInputStream(path + File.separator + "EncodedIV");
                FileInputStream keyEncoded = new FileInputStream(path + File.separator + "EncodedKey");

                byte[] keyBytes = asymmetricEngine.decrypt(IOUtils.toByteArray(keyEncoded));
                byte[] ivBytes = IOUtils.toByteArray(ivEncoded);
                SymmetricCryptoEngine engine = new SymmetricCryptoEngine(keyBytes,ivBytes);

                String decoded = engine.decrypt(IOUtils.toString(fileEncoded,"UTF-8"));
                FileUtils.writeStringToFile(new File(filePath),decoded,"UTF-8");

                fileEncoded.close();
                ivEncoded.close();
                keyEncoded.close();
                FileUtils.forceDelete(new File(path + File.separator + "EncodedIV"));
                FileUtils.forceDelete(new File(path + File.separator + "EncodedKey"));
                System.out.println("Decrypted and wrote everything to file");
                System.exit(0);
            } catch (IOException e){
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            System.out.println("Please specify a mode between ENCRYPT and DECRYPT");
            System.exit(2);
        }
    }

}